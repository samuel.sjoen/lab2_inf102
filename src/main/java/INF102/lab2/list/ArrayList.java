package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	public static final int DEFAULT_CAPACITY = 10;

	private int n;

	private Object elements[];

	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}

	@Override
	public T get(int index) {
		if (index >= n || index < 0) {
			throw new IndexOutOfBoundsException(index);
		} else {
			return (T) elements[index];
		}

	}

	@Override
	public void add(int index, T element) {
		if (index > n) {
			throw new IndexOutOfBoundsException(index);
		} else {
			int lengthOfArray = elements.length;
			if (lengthOfArray == n) {
				elements = Arrays.copyOf(elements, lengthOfArray * 2);
			}
			if (lengthOfArray > n) {
				for (int i = n; i > index; i--) {
					elements[i] = elements[i - 1];
				}
			}
			elements[index] = element;
			n += 1;
		}

	}

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n * 3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n - 1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}